import 'package:flutter/material.dart';
import 'package:flutter_app/bar/NewsList.dart';


class ChangeWidget extends StatefulWidget {
  ChangeWidget({Key key}) : super(key: key);

  @override
  _SampleAppPageState createState() => _SampleAppPageState();
}

class _SampleAppPageState extends State<ChangeWidget> {
  bool toggle = true;

  void _startPage() {
    Navigator.of(context).push(new PageRouteBuilder(
        pageBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return new NewsListState("测试");
        }));
  }

   _getChild() {
    if (toggle) {
      return Text('測試');
    } else {
      return MaterialButton(onPressed: () {}, child: Text('Toggle Two'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: _getChild()),
      floatingActionButton: FloatingActionButton(onPressed: _startPage,tooltip: 'update',  child: Icon(Icons.update),),
    );
  }
}
