import 'package:flutter/material.dart';

class NewsList extends State<NewsListState> with TickerProviderStateMixin {
  final title;
  final List<ListItem> listData = [];

  NewsList(this.title);

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < 20; i++) {
      listData.add(new ListItem("我是测试标题$i", Icons.cake));
    }
    return Scaffold(
      body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 300.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(
                    title,
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                    ),
                  ),
                  background: Image.network(
                    "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1531798262708&di=53d278a8427f482c5b836fa0e057f4ea&imgtype=0&src=http%3A%2F%2Fh.hiphotos.baidu.com%2Fimage%2Fpic%2Fitem%2F342ac65c103853434cc02dda9f13b07eca80883a.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SliverPersistentHeader(
                pinned: true,
                delegate: _SliverAppBarDelegate(
                  TabBar(
                      controller: new TabController(length: 2, vsync: this),
                      labelColor: Colors.black38,
                      unselectedLabelColor: Colors.grey,
                      tabs: [
                        Tab(icon: Icon(Icons.security), text: "热点"),
                        Tab(icon: Icon(Icons.cake), text: "科技"),
                      ]),
                ),
              ),
            ];
          },
          body: Center(
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return ListItemWidget(listData[index]);
              },
              itemCount: listData.length,
            ),
          )),
    );
  }
}

class ListItem {
  final String title;
  final IconData iconData;

  ListItem(this.title, this.iconData);
}

class ListItemWidget extends StatelessWidget {
  final ListItem listItem;

  ListItemWidget(this.listItem);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: ListTile(
        title: Text(listItem.title),
        leading: Icon(listItem.iconData),
      ),
    );
  }
}

class NewsListState extends StatefulWidget {
  final title;

  NewsListState(this.title);

  @override
  State<StatefulWidget> createState() => NewsList(title);
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar tabBar;

  _SliverAppBarDelegate(this.tabBar);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: tabBar,
      color: Colors.white,
    );
  }

  // TODO: implement maxExtent
  @override
  double get maxExtent => tabBar.preferredSize.height;

  // TODO: implement minExtent
  @override
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
