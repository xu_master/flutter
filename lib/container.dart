import 'package:flutter/material.dart';

class ContainerPage extends StatelessWidget {
  ContainerPage({this.title});

  // Widget子类中的字段往往都会定义为"final"

  final Widget title;

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 60.0,
      padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 20.0),
      decoration:
      BoxDecoration(image: DecorationImage(image: NetworkImage('http://pic.58pic.com/00/94/40/02bOOOPIC2e.jpg'))),
      transform: Matrix4.skewX(10.1),
      alignment: Alignment.center,
      child: new Row(
        children: <Widget>[
          new IconButton(
            icon: new Icon(Icons.menu),
            onPressed: null,
            tooltip: 'menu',
          ),
          new Expanded(child: title),
          new IconButton(
            icon: new Icon(Icons.search),
            tooltip: 'Search',
            onPressed: null,
          ),
        ],
      ),
    );
  }
}
class MyScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Material 是UI呈现的“一张纸”
    return new Material(
      // Column is 垂直方向的线性布局.
      child: new Column(
        children: <Widget>[
          new ContainerPage(
            title: new Text(
              'Example title',
            ),
          ),
          new Expanded(
            child: new Center(
              child: new Text('Hello, world!'),
            ),
          ),
        ],
      ),
    );
  }
}
