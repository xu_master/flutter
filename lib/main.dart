import 'package:flutter/material.dart';
import 'package:flutter_app/ChangeWidget.dart';
import 'package:flutter_app/container.dart';
import 'package:flutter_app/widget/DialogWidget.dart';
import 'package:flutter_app/widget/GridViewWidget.dart';
import 'package:flutter_app/widget/RowAndColumn.dart';
import 'package:flutter_app/widget/RowAndColumnTest.dart';
import 'package:flutter_app/widget/ScaffoldWidget.dart';
import 'package:flutter_app/widget/ScaffoldWidgetTest.dart';

void main() {
  runApp(new SampleApp());
}

class SampleApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new GridViewWidget(),
    );
  }
}

class SampleAppPage extends StatefulWidget {
  SampleAppPage({Key key}) : super(key: key);

  @override
  _SampleAppPageState createState() => new _SampleAppPageState();
}

class _SampleAppPageState extends State<SampleAppPage> {
  // Default value for toggle
  bool toggle = true;

  void _toggle() {
    setState(() {
      toggle = !toggle;
    });
  }

  _getToggleChild() {
    if (toggle) {
      return new Text('啥玩意');
    } else {
      return new MaterialButton(
          onPressed: () {}, child: new Text('Toggle Two'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("新闻"),
      ),
      body: new Center(
        child: _getToggleChild(),
      ),
//      bottomSheet: (new RawMaterialButton(
//        onPressed: () {},
//        child: new Text("首页"),
//      )),
//
      bottomNavigationBar: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new RawMaterialButton(
            onPressed: () {},
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new Text("首页"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          new RawMaterialButton(
            onPressed: () {},
            child: new Text("新闻"),
          ),
          new RawMaterialButton(
            onPressed: () {},
            child: new Text("我的"),
          ),
        ],
      ),

      floatingActionButton: new FloatingActionButton(
        onPressed: _toggle,
        tooltip: 'Update Text',
        child: new Icon(Icons.update),
      ),
    );
  }
}
