import 'dart:async';

import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DbUtils {
  static Future<String> get _dbPath async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "name.db");
    return path;
  }

  static Future<Database> get _localFile async {
    final path = await _dbPath;
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE user (id INTEGER PRIMARY KEY, name TEXT)");
    });
    return database;
  }

  static Future<int> save(String name) async {
    final db = await _localFile;
    return db.transaction((trx) {
      trx.rawInsert('INSERT INTO user(name) VALUES("$name")');
    });
  }

  static Future<List<Map>> get() async {
    final db = await _localFile;
    List<Map> list = await db.rawQuery('SELECT * FROM user');
    return list;
  }
}
