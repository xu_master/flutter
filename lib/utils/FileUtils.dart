import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class FileUtils {
  static Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<File> getFile(String fileName) async {
    return getFileBySuffix(fileName, "txt");
  }

  static Future<File> getFileBySuffix(
      String fileName, String _fileSuffix) async {
    final path = await _localPath;
    return new File('$path/$fileName.$_fileSuffix');
  }

  static Future<File> save(String fileName, String name) async {
    final file = await getFile(fileName);
    print(fileName);
    return file.writeAsString(name);
  }

  static Future<String> get(String fileName) async {
    final file = await getFile(fileName);
    return file.readAsString();
  }
}
