
import 'package:http/http.dart' as http;

class NetWorkUtils {
  var url = "http://example.com/whatsit/create";

  static void httpGet() {
    print("请求开始");
    http.get("https://api.github.com/users/flyou").then((response) {
      print("请求内容：" + response.body);
    }).catchError((error) {
      print("请求出错：" + error.toString());
    }).whenComplete(() {
      print("请求完成");
    });
  }
}
