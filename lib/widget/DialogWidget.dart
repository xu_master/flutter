import 'package:flutter/material.dart';

class DialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnackBarWidget();
  }
}

class SnackBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("DialogWidget"),
        ),
        body: Center(
          child: Builder(
            builder: (BuildContext context) {
              return RaisedButton(
                textColor: Colors.white,
                color: Colors.blueAccent,
                child: Text("按钮"),
                onPressed: () {
//                  Scaffold.of(context).showSnackBar(SnackBar(
//                        content: Text("消息来了"),
//                        action: SnackBarAction(label: "取消", onPressed: () {}),
//                      ));
//                  showDialog(
//                      context: context,
//                      builder: (BuildContext context) {
//                        return AboutDialogWidget();
//                      });
                  showModalBottomSheet(
                      context: context,

                      builder: (BuildContext context) {
                        return Container(
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              children: <Widget>[
                                new ListTile(
                                  leading: new Icon(Icons.chat),
                                  title: new Text("开始会话"),
                                ),
                                new ListTile(
                                  leading: new Icon(Icons.help),
                                  title: new Text("操作说明"),
                                ),
                                new ListTile(
                                  leading: new Icon(Icons.settings),
                                  title: new Text("系统设置"),
                                ),
                                new ListTile(
                                  leading: new Icon(Icons.more),
                                  title: new Text("更多设置"),
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                },
              );
            },
          ),
        ));
  }
}

class SimpleDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SimpleDialog(
      title: Text('对话框'),
      children: <Widget>[
        new ListTile(
          leading: new Icon(Icons.apps),
          title: new Text("apps"),
        ),
        new ListTile(
          leading: new Icon(Icons.android),
          title: new Text("andrpid"),
        ),
        new ListTile(
          leading: new Icon(Icons.cake),
          title: new Text("cake"),
        ),
        new ListTile(
          leading: new Icon(Icons.local_cafe),
          title: new Text("cofe"),
        ),
      ],
    );
  }
}

class AlertDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AlertDialog(
      title: Text('对话框'),
      content: Text("确定退出么？"),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("确定"),
        ),
      ],
    );
  }
}

class AboutDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new AboutDialog(
      applicationName: "最佳助手：",
      applicationVersion: "V1.0",
      applicationIcon: new Icon(
        Icons.android,
        color: Colors.blueAccent,
      ),
      children: <Widget>[new Text("更新摘要\n新增飞天遁地功能\n优化用户体验")],
    );
  }
}
