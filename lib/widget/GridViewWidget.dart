import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app/TutorialHome.dart';
import 'package:flutter_app/utils/DbUtils.dart';
import 'package:flutter_app/utils/FileUtils.dart';
import 'package:flutter_app/utils/NetWorkUtils.dart';
import 'package:flutter_app/utils/SPUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GridViewWidget extends StatelessWidget {
  final List<GridItem> listData = [];

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < 20; i++) {
      listData.add(new GridItem("我是测试标题$i", Icons.cake));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('GridView'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.0,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GridViewItem(listData[index]);
        },
        itemCount: listData.length,
      ),
    );
  }
}

class GridItem {
  final String title;
  final IconData iconData;

  GridItem(this.title, this.iconData);
}

class GridViewItem extends StatelessWidget {
  GridItem gridItem;

  GridViewItem(this.gridItem);

  SharedPreferences prefs;

  @override
  Widget build(BuildContext context) {
    SPUtils.save("test", "好哟");
//    DbUtils.save("张三");
    NetWorkUtils.httpGet();
    return new GestureDetector(
      child: new Container(
        color: Colors.blue,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(gridItem.iconData, size: 50.0),
            new Text(gridItem.title)
          ],
        ),
      ),
      onTap: () {
        FileUtils.save("t", '文件在哪');
        Future<Object> userName = SPUtils.get("test");
        FileUtils.getFile("t").then((File file) {
          file.readAsString().then((String value) {
            Scaffold
                .of(context)
                .showSnackBar(SnackBar(content: Text("数据获取成功：$value")));
          });
        });
        DbUtils.get().then((List<Map> list) {
          Scaffold
              .of(context)
              .showSnackBar(SnackBar(content: Text("数据获取成功：$list")));
        });
        userName.then((Object value) {
          Scaffold
              .of(context)
              .showSnackBar(SnackBar(content: Text("数据获取成功：$value")));
        });
      },
    );
  }
}
