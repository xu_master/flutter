import 'package:flutter/material.dart';

class RowAndColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: ColumnWidget(),
    );
  }
}

class RowWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        new Expanded(child: Text('热点',textAlign: TextAlign.center,),flex: 1,),
        new Expanded(child: Text('科技',textAlign: TextAlign.center,),flex: 1,),
        new Expanded(child: Text('社会',textAlign: TextAlign.center,),flex: 4,),
      ],
    );
  }
}
class ColumnWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
   return Column(
     children: <Widget>[
       new Expanded(child: new Icon(Icons.code),flex: 1,),
       new Expanded(child: new Icon(Icons.games),flex: 2,),
       new Expanded(child:  new Text("Hello Flutter"),flex: 3,),
     ],
   );
  }

}