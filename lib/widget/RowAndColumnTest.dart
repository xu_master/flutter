import 'package:flutter/material.dart';
import 'package:flutter_app/widget/StackWidget.dart';

class RowAndColumnTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("标题"),
      ),
      body:
          Container(margin: const EdgeInsets.all(10.0), child: StackWidget()),
    );
  }
}

class RowWidget extends StatelessWidget {
  IconData iconData;
  var name = "";

  RowWidget(this.name, this.iconData);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: new BoxDecoration(
        border: new Border(
            bottom: new BorderSide(
          color: Colors.grey,
          width: 0.3,
        )),
      ),
      height: 60.0,
      child: Row(
        children: <Widget>[
          Icon(
            iconData,
            color: Colors.lightBlueAccent,
          ),
          new Expanded(
            child: Text(
              name,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}

class ColumnWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RowWidget("消息记录", Icons.chat),
        RowWidget("我的收藏", Icons.stars),
        RowWidget("我的账户", Icons.lock),
        RowWidget("意见反馈", Icons.send),
        RowWidget("系统设置", Icons.settings),
      ],
    );
  }
}
