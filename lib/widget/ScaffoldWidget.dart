import 'package:flutter/material.dart';

class ScaffoldWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Pager();
}

class Pager extends State<ScaffoldWidget> {
  int currentPager = 0;
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool isCheck = false;
  int radioValue = 0;
  double currentPosition = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leading: Icon(Icons.account_circle),
        title: Text(
          '新闻',
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.red,
        elevation: 10.0,
        centerTitle: true,
      ),
      drawer: Drawer(
        child: new Column(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new NetworkImage(
                            "http://t2.hddhhn.com/uploads/tu/201612/98/st93.png"))),
                accountName: new Text("xf"),
                accountEmail: new Text("1692633552@qq.com"),
                currentAccountPicture: new CircleAvatar(
                  backgroundImage: new NetworkImage(
                      "http://tva2.sinaimg.cn/crop.0.3.707.707.180/a2f7c645jw8f6qvlbp1g7j20js0jrgrz.jpg"),
                )),
            new ListTile(
              leading: new Icon(Icons.refresh),
              title: new Text("刷新"),
            ),
            new ListTile(
              leading: new Icon(Icons.help),
              title: new Text("帮助"),
            ),
            new ListTile(
              leading: new Icon(Icons.chat),
              title: new Text("会话"),
            ),
            new ListTile(
              leading: new Icon(Icons.settings),
              title: new Text("设置"),
            ),
          ],
        ),
      ),
      body: new Container(
        margin: EdgeInsets.all(15.0),
        alignment: Alignment(0.0, 1.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            TextField(
              controller: _usernameController,
              keyboardType: TextInputType.number,
              maxLength: 11,
              decoration: new InputDecoration(
                contentPadding: const EdgeInsets.only(top: 20.0),
                icon: new Icon(Icons.perm_identity),
                labelText: "请输入用户名",
              ),
              autofocus: true,
            ),
            TextField(
              controller: _passwordController,
              maxLength: 16,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                contentPadding: const EdgeInsets.only(top: 20.0),
                icon: new Icon(Icons.lock),
                labelText: "请输入密码",
              ),
              autofocus: true,
            ),
            Row(
              children: <Widget>[
                Checkbox(
                  value: isCheck,
                  onChanged: onCheckChange,
                ),
                Text('记住密码')
              ],
            ),
            new Builder(builder: (BuildContext context) {
              //监听RaisedButton的点击事件，并做相应的处理
              return new MaterialButton(
                color: Colors.blueAccent,
                minWidth: 300.0,
                child: Text(
                  "登录",
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  if (_usernameController.text.toString() == "xf" &&
                      _passwordController.text.toString() == "123456") {
                    Scaffold
                        .of(context)
                        .showSnackBar(SnackBar(content: Text('登录成功')));
                    _clearText();
                  } else {
                    Scaffold.of(context).showSnackBar(
                        new SnackBar(content: new Text("登录失败，用户名密码有误")));
                  }
                },
                highlightColor: Colors.lightBlueAccent,
              );
            }),
            Radio(
              value: 0,
              groupValue: radioValue,
              onChanged: handleRadioValueChanged,
            ),
            Radio(
              value: 1,
              groupValue: radioValue,
              onChanged: handleRadioValueChanged,
            ),
            Radio(
              value: 2,
              groupValue: radioValue,
              onChanged: handleRadioValueChanged,
            ),
            Switch(
              value: isCheck,
              onChanged: onCheckChange,
            ),
            Slider(
              label: currentPosition.toString(),
              value: currentPosition,
              min: 0.0,
              max: 100.0,
              onChanged:onSliderChange,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), title: Text('购物车')),
          BottomNavigationBarItem(
              icon: new Icon(Icons.message), title: new Text("会话")),
          BottomNavigationBarItem(
              icon: new Icon(Icons.person), title: new Text("我的")),
        ],
        fixedColor: Colors.red,
        currentIndex: currentPager,
        onTap: (int index) {
          _selectCurrentPager(index);
        },
      ),
    );
  }

  _selectCurrentPager(final int index) {
    setState(() {
      currentPager = index;
    });
  }

  _clearText() {
    _usernameController.text = "";
    _passwordController.text = "";
  }

  onCheckChange(bool isChecked) {
    setState(() {
      isCheck = isChecked;
    });
  }

  void handleRadioValueChanged(int value) {
    setState(() {
      radioValue = value;
    });
  }

  onSliderChange(double position) {
    setState(() {
      print(position);
      currentPosition = position;
    });
  }

}
