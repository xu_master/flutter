import 'package:flutter/material.dart';

class ScaffoldWidgetTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Pager();
}

class Pager extends State<ScaffoldWidgetTest> {
  bool isCheck = false;
  int selectCurrent = 0;
  bool isOpen = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ScaffoldWidgetTest')),
      body: Container(
        margin: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            CheckboxListTile(
              onChanged: onCheckChange,
              value: isCheck,
              secondary: Icon(
                Icons.message,
                color: Colors.blueAccent,
              ),
              title: Text('新版本自动下载'),
              subtitle: Text('仅在WiFi环境下生效'),
            ),
            Divider(
              height: 1.0,
            ),
            RadioListTile(
              value: 0,
              controlAffinity: ListTileControlAffinity.trailing,
              groupValue: selectCurrent,
              secondary: Icon(
                Icons.timer,
                color: Colors.blueAccent,
              ),
              title: new Text("定时提醒间隔"),
              subtitle: new Text("10分钟"),
              onChanged: selectRadio,
            ),
            RadioListTile(
              value: 1,
              controlAffinity: ListTileControlAffinity.trailing,
              groupValue: selectCurrent,
              secondary: Icon(
                Icons.timer,
                color: Colors.blueAccent,
              ),
              title: new Text("定时提醒间隔"),
              subtitle: new Text("30分钟"),
              onChanged: selectRadio,
            ),
            RadioListTile(
              value: 2,
              controlAffinity: ListTileControlAffinity.trailing,
              groupValue: selectCurrent,
              secondary: Icon(
                Icons.timer,
                color: Colors.blueAccent,
              ),
              title: new Text("定时提醒间隔"),
              subtitle: new Text("1小时"),
              onChanged: selectRadio,
            ),
            Divider(
              height: 1.0,
            ),
            SwitchListTile(
              secondary: Icon(
                Icons.message,
                color: Colors.blueAccent,
              ),
              value: isOpen,
              onChanged: swich,
            ),
          ],
        ),
      ),
    );
  }

  onCheckChange(bool isChecked) {
    setState(() {
      isCheck = isChecked;
    });
  }

  void selectRadio(int index) {
    setState(() {
      selectCurrent = index;
    });
  }

  swich(bool isOpen) {
    setState(() {
      this.isOpen = isOpen;
    });
  }
}
