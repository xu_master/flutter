import 'package:flutter/material.dart';

class StackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IndexStackWidget();
  }
}

class IndexStackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      children: <Widget>[
        new Icon(
          Icons.check_circle,
          size: 100.0,
          color: Colors.green,
        ),
        new Icon(
          Icons.error_outline,
          size: 100.0,
          color: Colors.red,
        )
      ],
      alignment: Alignment.bottomRight,
    );
  }
}

class StackWidgetTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Icon(
          Icons.check_circle,
          size: 100.0,
          color: Colors.yellow,
        ),
        Icon(
          Icons.check_circle,
          color: Colors.lightGreen,
        )
      ],
      alignment: Alignment.bottomRight,
    );
  }
}
